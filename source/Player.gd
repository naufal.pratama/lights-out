extends "Actor.gd"

export (PackedScene) var Bullet
export (float) var muzzle_length = 100
export (float) var max_charge_time = 3.0

export (float) var bullet_small_size = 0.6
export (float) var bullet_big_size = 2

signal player_fired_bullet(bullet, position, direction, size)
signal player_become_visible()
signal player_become_invisible()

onready var wand = $Wand
onready var bullet_point = $Wand/BulletPoint
onready var wand_anim = $Wand/AnimatedSprite
onready var invul_time = $InvulnerabilityTime
onready var visi_time = $VisibilityTime
onready var effects = $EffectsAnimation
onready var walk_effect = $AudioEffect/WalkingSound
onready var hit_effect = $AudioEffect/HitSound

var is_charging = false
var bullet_size = bullet_small_size
var is_walking = false

func _ready():
	$Wand/Light2D.texture_scale = 0
	$AnimatedSprite.play("Idle")

func _physics_process(delta) -> void:
	
	var movement_direction := Vector2.ZERO
	if(Input.is_action_pressed("up")):
		movement_direction.y -= 1
	if(Input.is_action_pressed("down")):
		movement_direction.y += 1
	if(Input.is_action_pressed("left")):
		movement_direction.x -= 1
	if(Input.is_action_pressed("right")):
		movement_direction.x += 1

	if is_walking and movement_direction == Vector2.ZERO:
		walk_effect.stop()
		is_walking = false
	elif not is_walking and movement_direction != Vector2.ZERO:
		walk_effect.play()
		is_walking = true
	
	var animation = "Idle"
	if movement_direction != Vector2.ZERO:
		animation = "Walk"

	movement_direction = movement_direction.normalized()
	move_and_slide(movement_direction * movement_speed)
	
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
	
	var target = get_global_mouse_position()
	var direction_to_mouse = global_position.direction_to(target)
	wand.rotation = direction_to_mouse.angle()

	if direction_to_mouse.x < 0:
		$AnimatedSprite.flip_h = true
	if direction_to_mouse.x > 0:
		$AnimatedSprite.flip_h = false

func _unhandled_input(event) -> void:
	if(event.is_action_pressed("shoot")):
		if !is_charging:
			setup_shot()
	if(event.is_action_released("shoot")):
		shoot()

func setup_shot():
	wand_anim.play("On")
	$Wand/Light2D.texture_scale = 0
	is_charging = true
	$ChargeTime.start(max_charge_time)
	bullet_size = bullet_small_size

func shoot():
	wand_anim.play("Off")
	is_charging = false
	$Wand/Light2D.texture_scale = 0
	$ChargeTime.stop()
	visi_time.start()
	var bullet_instance = Bullet.instance()
	var target = get_global_mouse_position()
	var direction_to_mouse = global_position.direction_to(target)
	emit_signal("player_fired_bullet", bullet_instance, bullet_point.global_position, direction_to_mouse, bullet_size)

func _on_ChargeTime_timeout():
	if is_charging:
		$Wand/Light2D.texture_scale = 1
		bullet_size = bullet_big_size
		emit_signal("player_become_visible")
		
func take_damage(damage):
	if invul_time.is_stopped():
		hit_effect.play()
		$SoundTime.start()
		invul_time.start()
		.take_damage(damage)
		effects.play("damage")
		effects.queue("flash")

func _on_InvulnerabilityTime_timeout():
	effects.play("rest")

func _on_VisibilityTime_timeout():
	emit_signal("player_become_invisible")

func _on_SoundTime_timeout():
	hit_effect.stop()
