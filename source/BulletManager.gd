extends Node2D

func handle_bullet_spawned(bullet, position, direction, size):
	bullet.global_position = position
	bullet.set_direction(direction)
	bullet.rotation = direction.angle()
	bullet.set_size(size)
	add_child(bullet)
	bullet.start()
