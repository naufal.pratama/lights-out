extends Node2D

func _ready():
	pass

func _physics_process(delta):
	var target = get_global_mouse_position()
	$Follower.position = target
	pass
	
func _on_Start_pressed():
	get_tree().change_scene("res://scenes/Before1.tscn")

func _on_Exit_pressed():
	get_tree().quit()
