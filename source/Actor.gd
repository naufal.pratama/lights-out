extends KinematicBody2D

export (int) var movement_speed = 100
export (float) var max_health = 100

signal health_updated(health)
signal player_killed()

onready var health = max_health setget _set_health

func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		if self.name == "Player":
			emit_signal("health_updated", health)
		if health == 0:
			dies()

func _ready():
	pass

func dies():
	if self.name == "Player":
		print("Player Dies")
		emit_signal("player_killed")

func take_damage(damage):
	_set_health(health - damage)
