extends Node2D

export (String) var sceneName = "Main.tscn"

func _ready():
	pass

func _on_TextureButton_pressed():
	get_tree().change_scene("res://scenes/" + sceneName)
