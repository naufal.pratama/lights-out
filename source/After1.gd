extends Node2D

export (String) var retryName = "Main.tscn"

func _ready():
	pass

func _on_Retry_pressed():
	get_tree().change_scene("res://scenes/" + retryName)

func _on_MainMenu_pressed():
	get_tree().change_scene("res://scenes/TitleScreen.tscn")
