extends Area2D

export (PackedScene) var Explosion

export (int) var bullet_speed = 300
export (int) var damage_small = 5
export (int) var damage_big = 20
export (float) var lifetime = 1.0
export (float) var factor = 1.0

var bullet_direction := Vector2.ZERO
var damage

func start():
	$Lifetime.start(lifetime)
	$AnimatedSprite.play("default")

func _physics_process(delta) -> void:
	if (bullet_direction != Vector2.ZERO):
		var velocity = bullet_direction * bullet_speed * delta
		global_position += velocity

func set_size(size) -> void:
	scale = Vector2(size, size)
	if (size < 1):
		damage = damage_small
		$Light2D.texture_scale = size
	else:
		damage = damage_big
		$Light2D.texture_scale = size * factor

func set_direction(direction: Vector2) -> void:
	self.bullet_direction = direction

func explode():
	var explosion_instance = Explosion.instance()
	if damage == damage_small:
		explosion_instance.scale = Vector2(0.5, 0.5)
	else:
		explosion_instance.scale = Vector2(1, 1)
	explosion_instance.position = get_global_position()
	get_tree().get_root().add_child(explosion_instance)
	queue_free()

func _on_Lifetime_timeout():
	explode()

func _on_Bullet_body_entered(body):
	explode()
	if body.has_method('take_damage'):
		body.take_damage(damage)
