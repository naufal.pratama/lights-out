extends "Actor.gd"

export (PackedScene) var DeathAnim

export (int) var run_speed = 1000
export (int) var damage
export (float) var min_distance = 100
export (float) var max_distance = 500

onready var nav = get_tree().get_root().get_node("Main/Navigation2D") setget set_nav
onready var speed = movement_speed

var path = []
var goal = Vector2()

var state = 1
var prev_state = 1

var player_is_visible = false

func get_random_direction() -> Vector2:
	var res = Vector2.ZERO
	res.x = rand_range(-1, 1)
	res.y = rand_range(-1, 1)
	res = res.normalized()
	return res

func get_roam_position() -> Vector2:
	var direction = get_random_direction()
	return global_position + direction * rand_range(min_distance, max_distance)

func get_player_position() -> Vector2:
	return get_tree().get_root().get_node("Main/Player").get_global_position()

func _ready():
	$AnimatedSprite.play("default")

func set_nav(new_nav):
	nav = new_nav
	update_path()
	
func update_path():
	goal = get_roam_position()
	path = nav.get_simple_path(global_position, goal, false)
	
func update_chase():
	goal = get_player_position()
	path = nav.get_simple_path(global_position, goal, false)
	
func _physics_process(delta):
	var collisionCounter = get_slide_count() - 1
	if collisionCounter > -1:
		var collision = get_slide_collision(collisionCounter)
		if collision.collider.name == "Player":
			get_tree().get_root().get_node("Main/Player").take_damage(damage)
	var player_is_visible = get_tree().get_root().get_node("Main").get_player_visibility()
	var player_pos = get_player_position()
	if position.distance_to(player_pos) < 800 and player_is_visible:
		state = 2
	else:
		state = 1
	if prev_state != state:
		while path.size() > 0:
			path.remove(0)
		if state == 1:
			update_path()
			speed = movement_speed
		if state == 2:
			update_chase()
			speed = run_speed
		prev_state = state
	roam()

func roam():
	if path.size() > 1:
		var d = global_position.distance_to(path[0])
		if d > 10:
			var movement_direction = global_position.direction_to(path[0])
			movement_direction = movement_direction.normalized()
			if movement_direction.x > 0:
				$AnimatedSprite.flip_h = true
			if movement_direction.x < 0:
				$AnimatedSprite.flip_h = false
			move_and_slide(movement_direction * speed)
		else:
			path.remove(0)
	else:
		if state == 1:
			update_path()
		if state == 2:
			update_chase()

func dies():
	var death_instance = DeathAnim.instance()
	death_instance.position = get_global_position()
	get_tree().get_root().get_node("Main/EnemyGrave").add_child(death_instance)
	death_instance.play("default")
	get_tree().get_root().get_node("Main").enemy_killed()
	queue_free()

func take_damage(damage):
	.take_damage(damage)
	$RunningTimer.start()
	if state == 1:
		speed = run_speed

func _on_RunningTimer_timeout():
	if state == 1:
		speed = movement_speed
		
