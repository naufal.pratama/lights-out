extends Node2D

onready var bullet_manager = $BulletManager
onready var player = $Player
onready var health_bar = $HUD/HealthBar
onready var player_visibility = false

export (int) var enemy_number
export (String) var loseName = "After1.tscn"
export (String) var winName = "WinScene.tscn"

onready var killed_enemy = 0

func _ready() -> void:
	player.connect("player_fired_bullet", bullet_manager, "handle_bullet_spawned")
	player.connect("player_become_visible", self, "handle_player_visible")
	player.connect("player_become_invisible", self, "handle_player_invisible")
	player.connect("health_updated", health_bar, "update_health")
	player.connect("player_killed", self, "lose_scene")

func handle_player_visible():
	player_visibility = true

func handle_player_invisible():
	player_visibility = false

func get_player_visibility():
	return player_visibility

func lose_scene():
	get_tree().change_scene("res://scenes/" + loseName)

func enemy_killed():
	killed_enemy += 1
	if killed_enemy == enemy_number:
		get_tree().change_scene("res://scenes/" + winName)
